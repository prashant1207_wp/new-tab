## iOS Specific

### References:

1. https://www.fadel.io/blog/posts/ios-performance-tips-you-didnt-know/

### 1. Performance with UILabel

We're tempted to think of labels as lightweight views in terms of memory. In the end, they just display text. UILabels are actually stored as bitmaps, which could easily consume megabytes of memory.
Thankfully, the UILabel implementation is smart, and only consumes what it needs to:

* If your label is monochrome, UILabel would opt for CALayerContentsFormat of kCAContentsFormatGray8Uint (1 byte per pixel), whereas non-monochrome labels (e.g. to display "🥳it's party time", or a multi-colored NSAttributedString) would need to use kCAContentsFormatRGBA8Uint (4 bytes per pixel).

* A monochrome label consumes a maximum of width * height * contentsScale^2 * (1 byte per pixel) bytes
* A non-monochrome one would consume 4 times as much: width * height * contentsScale^2 * (4 bytes per pixel).

For example, on an iPhone 11 Pro Max, a label of size 414 * 100 points could consume up to:

414 * 100 * 3^2 * 1 = 372.6kB (monochrome)

414 * 100 * 3^2 * 4 = ~1.49MB (non-monochrome)

Here's how you can instantly free up multiple megabytes of memory:

- If you have labels that you sometimes set to hidden, set their text value to nil when they are no longer visible.

- Nilify your labels’ text contents in -prepareForReuse if they're displayed in UITableView/UICollectionView cells.


### 2. In most cases, serial queues should be enough; Consider replacing all concurrent queues with serial queues.

### 3. Don't use UIView tags
UIKit implements tags using objc_get/setAssociatedObject(), meaning that every time you set or get a tag, you're doing a dictionary lookup, which will show up in Instrumentation.

## React Native Specific

### References:
1. https://hackernoon.com/react-native-performance-do-and-dont-1198e97b730a

### 1. Image Control 
React Native's Image component handles image caching like browsers for the most part. If the server is returning proper cache control headers for images you'll generally get the sort of built in caching behavior you'd have in a browser. Even so many people have noticed:
- Flickering.
- Cache misses.
- Low performance loading from cache.
- Low performance in general.

We should scale image on the server side rather than on the client.

### 2. PureComponent

PureComponent is a way to prevent a component to re-render when his props did not changed. More precisely it implements it implements shouldComponentUpdate by doing a shallow comparison of props.
- Don't create any new props at render time when using a pure component

### 3. Objects
When switching your component to a more functional style, be always aware that most functional function, which are in themselves pure, create new references. Once again, don't call theses functions at render time.

### 4. Architecture
- React Native is essentially two realms, one is JSCore and other is Native.
- These two realms pass messages to each other to perform operations.
- JSExecute -> Shadow Message Queue -> Native Operations.
- Message are passed as JSON object and thus has to be valid JSON objects only.
- native_operations = EvaluateScript('execute([js_operations])')
