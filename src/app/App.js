import React from 'react';
import Home from '../hooks/home/home.component';
import './App.css';
import 'antd/dist/antd.css';

function App() {
  return (
    <Home />
  );
}

export default App;
