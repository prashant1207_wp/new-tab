class StorageService {
    static storage_key = 'saved';
    static store = window.localStorage;
    static setItem = (key, item) =>  {
        this.store.setItem(key, item);
    }

    static getItem = (key) =>  {
        return this.store.getItem(key);
    }

    static deleteAllSavedItems = () => {
        this.store.removeItem(this.storage_key)
    }

    static getSavedItems = () => {
        let items = []
        let stored = this.getItem(this.storage_key)
        if (stored != null) {
            items = JSON.parse(stored)
        }
         
        return items;
    }

    static updateSavedItem = (value) => {
        let items = []
        let stored = this.getItem(this.storage_key)
        if (stored != null) {
            items = JSON.parse(stored)
        }
        
        let model = {
            value: value,
            time: Date.now(),
        }

        items.push(model)
        StorageService.saveJSON(this.storage_key, items)
    }

    static saveJSON(key, items) {
        StorageService.setItem(this.storage_key, JSON.stringify(items))
    }

    static deleteSavedItem = (item) => {
        let items = []
        let stored = this.getItem(this.storage_key)
        if (stored != null) {
            items = JSON.parse(stored)
        }

        let idx = items.findIndex(element => element.value === item.value && element.time === item.time)
        items.splice(idx, 1)
        
        StorageService.saveJSON(this.storage_key, items)
    }
}

export default StorageService;