class Utilities {
    static timeSince(date) {
        let seconds = Math.floor((new Date() - date) / 1000);
        let interval = Math.floor(seconds / 31536000);
        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }

        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes";
        }

        return Math.floor(seconds) + " seconds";
    }

    static updateGreeting() {
        let today = new Date();
        let curHr = today.getHours()
        let message = ''
        if (curHr < 12) {
            message = 'Good Morning'
        } else if (curHr < 18) {
            message = 'Good Afternoon'
        } else {
            message = 'Good Evening'
        }

        this.setState({
            message: message
        })
    }

    static formatTime(date) {
        const time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true })
        return time
    }

    static formatDate(date) {
        return date.toLocaleString('en-US', { weekday: 'long', month: 'long', day: 'numeric' })
    }
}

export default Utilities;