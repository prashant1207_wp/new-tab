import React, { useEffect, useState, useRef, useReducer } from "react";
import { Row, Col, Button } from 'antd';
import StorageService from '../../services/storage/storage-service';
import Reducer from '../reducer/reducer';
import Greetings from '../components/greetings/greetings.component';
import Shell from '../components/shell/shell.component';
import Grid from '../components/grid/grid.component';
import './home.component.css';

const reducer = new Reducer()

function HomeComponent() {
    const [savedItems, setItems] = useState([]);
    const [lastUpdate, setLastUpdate] = useState(Date.now());
    const [state, dispatch] = useReducer(reducer.savedItemReducer, { lastUpdate: Date.now() });

    useEffect(() => {
        setItems(StorageService.getSavedItems());
    }, [lastUpdate, state])

    return (
        <div className='home-page'>
            <Col span={12}>
                <Row type='flex' justify='start'>
                    <div className='left-panel'>
                        <Button title='Refresh' onClick={() => dispatch({ type: 'refresh' })}>Refresh</Button>
                        <div style={{
                            fontSize: 32,
                            color: 'orange'
                        }}>{state.lastUpdate}</div>
                        <div style={{
                            fontSize: 32,
                            color: 'yellow'
                        }}>{lastUpdate}</div>
                        <Shell setLastUpdate={setLastUpdate} />
                        <br />
                        <Grid items={savedItems} setLastUpdate={setLastUpdate} />
                    </div>
                </Row>
            </Col>
            <Col span={12}>
                <Row type='flex' justify='end'>
                    <Greetings />
                </Row>
            </Col>
        </div>
    );
}

export default HomeComponent;