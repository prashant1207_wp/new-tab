import React, { useState, useEffect } from 'react';
import { Row, Col, Icon } from 'antd';
import Utilities from '../../../services/utils/utils';
import './greetings.component.css';

function Greetings() {
    const [time, setTime] = useState(Utilities.formatTime(new Date()));
    const [date, setDate] = useState(Utilities.formatDate(new Date()));
    const [trigger, setTrigger] = useState(Date.now());

    useEffect(() => {
        setTime(Utilities.formatTime(new Date()));
        setDate(Utilities.formatDate(new Date()));
    }, [trigger])

    setTimeout(function () {
        setTrigger(Date.now())
    }, 1000)

    return (
        <div className='greetings'>
            <div style={{
                fontSize: '5rem',
                fontWeight: 600
            }}>{time}</div>
            <div style={{
                fontSize: '1rem',
                fontWeight: 400
            }}>{date}</div>
        </div>
    )
}

export default Greetings;
