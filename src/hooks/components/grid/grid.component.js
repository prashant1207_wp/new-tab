import React, { useState, useEffect } from 'react';
import { Card, List, Icon } from 'antd';
import StorageService from '../../../services/storage/storage-service';
import Utilities from '../../../services/utils/utils';
import './grid.component.css';

function Grid({ items, setLastUpdate }) {
    function onCardClicked(item) {
        const value = item.value;
        if (value.startsWith('http')) {
            window.open(value);
        }
    }

    function onDeleteClicked(item) {
        StorageService.deleteSavedItem(item)
        setLastUpdate(Date.now())
    }

    function renderCard(item) {
        let itemDate = new Date(item.time)
        let timeStamp = Utilities.timeSince(itemDate)
        return (
            <List.Item>
                <Card
                    actions={[
                        <Icon type="delete" onClick={value => onDeleteClicked(item)} />,
                        <Icon type="link" onClick={value => onCardClicked(item)} />
                    ]}
                    bordered>
                    <Card.Meta title={item.value} description={timeStamp} />
                </Card>
            </List.Item>)
    }

    return (
        <div className='grid-view'>
            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 1,
                    md: 1,
                    lg: 1,
                    xl: 1,
                    xxl: 1,
                }}
                dataSource={items}
                renderItem={item => renderCard(item)}
            />
        </div>
    )
}

export default Grid;