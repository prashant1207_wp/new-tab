import React, { useEffect, useState } from "react";
import { Input } from 'antd';
import StorageService from '../../../services/storage/storage-service';
import './shell.component.css';

function Shell({ setLastUpdate }) {
    const [inputValue, setInputValue] = useState(null)

    function onValueChanged(e) {
        setInputValue(e.target.value);
    }

    function onPressEnter(e) {
        StorageService.updateSavedItem(e.target.value)
        setInputValue(null);
        setLastUpdate(Date.now())
    }

    return (
        <div className='shell'>
            <Input
                value={inputValue}
                size='large'
                onChange={e => onValueChanged(e)}
                onPressEnter={e => onPressEnter(e)}
                placeholder="type anything" />
        </div>
    )
}

export default Shell;