import React, { useReducer, useState, useRef } from "react";

class Reducer {
    savedItemReducer = (state, action) => {
        let result = {}
        switch (action.type) {
            case 'refresh':
                result.lastUpdate = Date.now();
                break;
            default:
                result = state;
                break;
        }

        return result;
    }
}

export default Reducer;